# DarkStrawberry 

Dark crystal-look GTK2/3 theme. Tastes like Debian.

Original mint-flavored version by originalseed: https://github.com/originalseed/darkmint

![example screenshot](/example.png)

## Installation

```sh
$ git clone https://gitlab.com/machinefossil/darkstrawberry
$ cp -r darkstrawberry ~/.themes/
$ cp userContent.css ~/.mozilla/firefox/<bla_bla>/chrome # optional, installs Firefox theme
```
